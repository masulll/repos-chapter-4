class SuitGame {
  constructor() {
    if (this.constructor === SuitGame) {
      throw new Error("INI ADALAH ABSTRACT CLASS");
    }
  }

  refresh() {}

  determineTheWinner(p1choice, comchoice) {
    if (this.p1choice === this.comchoice) {
      console.log("DRAW");
    }
    switch (this.p1choice + this.comchoice) {
      case "batugunting":
      case "guntingkertas":
      case "kertasbatu": {
        console.log("win");
        return;
      }
      case "guntingbatu":
      case "kertasgunting":
      case "batukertas": {
        console.log("lose");
        return;
      }
    }
  }

  static getRandomValue() {
    let arr = ["batu", "gunting", "kertas"];
    let x = Math.floor(Math.random() * 3);
    return arr[x];
  }
}

class Player extends SuitGame {
  constructor(tag, value) {
    super(value, "");
    this.tag = tag;
    this.value = value;
    this.tag._value = _value;
  }
  choose(e) {
    console.log(e.target._value, "<<< Value");
    let _tag = e.target;
    //   console.log(_tag.style.border);
    if (_tag.style.border == "1px solid blue") {
      _tag.style.border = "";
      isCanPlay = true;
    } else {
      if (isCanPlay) {
        _tag.style.border = "1px solid blue";
        let comchoice = SuitGame.getRandomValue();
        let result = resultSuit.determineTheWinner(_tag._value, comchoice);
        resultSuit.showResult(result);
        console.log(comchoice, "<<< pilihan COM");
        // console.log(e.target);
      }
      isCanPlay = false;
    }
    //   console.log(isCanPlay, "FLAG2");
  }
  onClick() {
    this.tag.addEventListener("click", this.choose);
  }
}

class ResultSuit extends SuitGame {
  constructor(tag) {
    super();
    this.tag = tag;
  }
  showResult(result) {
    this.tag.innerHTML = result;
  }
}
let isCanPlay = true;

const card = document.querySelector("div#player img");
const resultSuit = new ResultSuit(document.querySelector("#result"));

const card1 = new Player(card[0], "batu");
const card2 = new Player(card[1], "gunting");
const card3 = new Player(card[2], "kertas");

img1.onClick();
img2.onClick();
img3.onClick();
